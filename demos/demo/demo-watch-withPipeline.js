'use strict';

const common = require('./common');
const Consumer = require('./consumer');
const CartStatus = require('./cart-status');

const analytics = new Consumer("analytics", false);


(async function () {

    try {
        const collection = await common.getCollection('cart');

        const options = { fullDocument: 'updateLookup' };

        const pipeline = [{ $match: { 'fullDocument.status': CartStatus.SHOP } }];

        // syntactic sugar, in the driver, it sets up an aggregation pipeline,
        // starts a cursor and sets up java script event handling
        const changeStream = collection.watch(pipeline, options);

        changeStream.on('change', event => {
            analytics.process(event);
        });

        // trap change stream errors here
        changeStream.on('error', err => {
            console.log(err);
            changeStream.close();
            throw err;
        })

    } catch (err) {
        console.log(err.stack);
    }
})()